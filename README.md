# Unreal Engine 4 On Linux

[Unreal Engine](https://www.unrealengine.com/) 4 on Linux.

WARNING! This is very much a WIP. Will update frequently! Proceed with caution.


## About

This is my repository for all of my needs for anything related to setup,
installing, developing or otherwise anything related to UE4 on Linux.
Specifically [Arch Linux](https://archlinux.org/).


## Unreal Engine 5

Currently, as of time of writing this. I can not seem to find any information
about how to setup, install, and run UE5 on Linux. It might actually be
possible, I just haven't come across any information on how yet. When I do, or
when it is possible, I will start a different repository for UE5 specifically,
as I am sure they are not 100% compatible, or the steps to setup will be
different.

The [UE4 AUR package](https://aur.archlinux.org/packages/unreal-engine/) does
seem to mention that it will be updated to UE5 once available, and comment shows
how someone modified the `PKGBUILD` scripts to install UE5, but is currently
getting errors. Maybe if I have some time to give it a try I might have better
luck.


## Setup / Install


### Prep

You can not install without first signing up. [https://wiki.archlinux.org/index.php/Unreal_Engine_4](https://wiki.archlinux.org/index.php/Unreal_Engine_4).

You can follow [unreal's how to guide](https://www.unrealengine.com/en-US/ue4-on-github)
on how exactly to signup and get connected to their Github source code repository.


### Install

Now you should be ready to actually install UE4.

```bash
yay -S unreal-engine
```

Or your choice of AUR tools.


## Documentation

* [Official Arch Wiki](https://wiki.archlinux.org/index.php/Unreal_Engine_4)
* [Unreal's Github Instructions](https://www.unrealengine.com/en-US/ue4-on-github)
* [Unreal's Github](https://github.com/EpicGames)
* [Unreal's AUR Package](https://aur.archlinux.org/packages/unreal-engine/)

